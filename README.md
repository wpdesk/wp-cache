[![pipeline status](https://gitlab.com/wpdesk/wp-cache/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-cache/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/wp-cache/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-cache/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/wp-cache/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-cache/commits/master)

WP Cache
========

Internal WP Desk Library used by wP Desk plugins. 
